package com.iteco.linealex.jse.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class AbstractEntity {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

}
