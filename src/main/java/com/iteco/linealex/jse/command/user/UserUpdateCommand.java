package com.iteco.linealex.jse.command.user;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-update";
    }

    @NotNull
    @Override
    public String description() {
        return "UPDATE USER'S PASSWORD";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER OLD PASSWORD");
        final String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD");
        final String newPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN");
        if (newPassword.equals(serviceLocator.getTerminalService().nextLine())) {
            serviceLocator.getUserService().updateUserPassword(oldPassword, newPassword,
                    serviceLocator.getSelectedEntityService().getSelectedUser());
            System.out.println("[OK]\n");
        } else System.out.println("MISTAKE IN THE REPEATING OF NEW PASSWORD\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
