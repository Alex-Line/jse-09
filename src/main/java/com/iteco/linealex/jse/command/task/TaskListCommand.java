package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL TASKS IN THE SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        Collection<Task> collection = null;
        if (serviceLocator.getSelectedEntityService().getSelectedProject() != null) {
            collection = serviceLocator.getTaskService().getAllEntities(
                    serviceLocator.getSelectedEntityService().getSelectedProject().getId(), selectedUser.getId());
        } else collection = serviceLocator.getTaskService().getAllEntities(selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
            return;
        }
        System.out.println("[TASK LIST]");
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}
