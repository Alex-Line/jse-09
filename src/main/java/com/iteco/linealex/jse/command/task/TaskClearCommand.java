package com.iteco.linealex.jse.command.task;

import com.iteco.linealex.jse.command.AbstractCommand;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASKS FROM THE SELECTED PROJECT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        @Nullable final Project selectedProject = serviceLocator.getSelectedEntityService().getSelectedProject();
        Collection<Task> collection = null;
        if (selectedProject == null) {
            collection = Collections.EMPTY_LIST;
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
        } else collection = serviceLocator.getTaskService().removeAllTasksFromProject(
                selectedProject.getId(), selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
