package com.iteco.linealex.jse.enumerate;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMINISTRATOR("administrator"),
    ORDINARY_USER("ordinary user");

    private String name;

    Role(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }

}
