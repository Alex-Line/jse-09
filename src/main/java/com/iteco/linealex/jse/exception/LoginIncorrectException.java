package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class LoginIncorrectException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY LOGIN. COMMAND WAS INTERRUPTED\n";
    }

}
