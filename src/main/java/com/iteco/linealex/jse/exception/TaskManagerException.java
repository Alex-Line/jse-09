package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class TaskManagerException extends Exception {

    @NotNull
    @Override
    public String getMessage() {
        return "[UNDEFINED TASK MANAGER EXCEPTION. COMMAND WAS INTERRUPTED]\n";
    }
}
