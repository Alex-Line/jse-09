package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class ShortPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "ENTERED PASSWORD IS TO SHORT. COMMAND WAS INTERRUPTED\n";
    }

}
