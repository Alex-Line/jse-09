package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class InsetExistingEntityException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THERE IS SUCH ELEMENT ALREADY. COMMAND WAS INTERRUPTED\n";
    }
}
