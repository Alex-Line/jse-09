package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class WrongDateFormatException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "[WRONG DATE FORMAT! COMMAND WAS INTERRUPTED]\n";
    }

}
