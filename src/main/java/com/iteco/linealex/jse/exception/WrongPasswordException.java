package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class WrongPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY PASSWORD. COMMAND WAS INTERRUPTED\n";
    }

}
