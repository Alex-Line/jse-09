package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class UserIsNotExistException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "USER IS NOT EXIST. COMMAND WAS INTERRUPTED\n";
    }
}
