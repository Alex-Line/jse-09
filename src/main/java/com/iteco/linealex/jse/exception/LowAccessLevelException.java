package com.iteco.linealex.jse.exception;

import org.jetbrains.annotations.NotNull;

public class LowAccessLevelException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU DO NOT HAVE RIGHTS TO DO THAT. PLEASE CONTACT ADMINISTRATOR FOR MORE INFORMATION. COMMAND WAS INTERRUPTED\n";
    }
}
