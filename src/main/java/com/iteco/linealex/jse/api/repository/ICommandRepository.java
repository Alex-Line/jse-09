package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandRepository {

    public void addCommand(AbstractCommand command);

    @Nullable
    public AbstractCommand getCommand(String command);

    @NotNull
    public Collection<AbstractCommand> getAllCommands();

}
