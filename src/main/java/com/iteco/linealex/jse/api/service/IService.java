package com.iteco.linealex.jse.api.service;

import com.iteco.linealex.jse.exception.InsetExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    @Nullable
    public T selectEntity(@Nullable final String entityName,
                          @Nullable final String userId);

    @Nullable
    public T persist(@Nullable final T entity) throws InsetExistingEntityException;

    @Nullable
    public T merge(@Nullable final T entity);

    @NotNull
    public Collection<T> getAllEntities();

    @Nullable
    public T removeEntity(@Nullable final String entityName);

}
