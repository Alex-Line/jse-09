package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.repository.ICommandRepository;
import com.iteco.linealex.jse.api.service.ICommandService;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository repository;

    public CommandService(@NotNull final ICommandRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addCommand(@NotNull final AbstractCommand command){
        repository.addCommand(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@NotNull final String command){
        return repository.getCommand(command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return repository.getAllCommands();
    }

}
