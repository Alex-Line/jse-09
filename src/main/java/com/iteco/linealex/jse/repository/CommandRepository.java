package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.ICommandRepository;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private Map<String, AbstractCommand> commands = new HashMap<>();

    @Override
    public void addCommand(@NotNull final AbstractCommand command) {
        commands.put(command.command(), command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@NotNull final String command) {
        for (Map.Entry<String, AbstractCommand> entry : commands.entrySet()) {
            if(entry.getValue().command().equals(command)) return entry.getValue();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return commands.values();
    }

}
