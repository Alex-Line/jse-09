package com.iteco.linealex.jse.repository;

import com.iteco.linealex.jse.api.repository.IRepository;
import com.iteco.linealex.jse.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements IRepository<Task> {

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String projectId,
                                    @NotNull final String userId) {
        @NotNull final Collection<Task> collection = new LinkedList<>();
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null || !entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                collection.add(entry.getValue());
            }
        }
        return collection;
    }

    @NotNull
    public Collection<Task> findAll(@NotNull final String userId) {
        final Collection<Task> collection = new LinkedList<>();
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String taskName,
                        @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            return entry.getValue();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String taskName,
                        @NotNull final String projectId,
                        @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            return entry.getValue();
        }
        return null;
    }

    @Nullable
    @Override
    public Task persist(@NotNull final Task example) {
        if (example.getName() == null) return null;
        if (example.getUserId() == null) return null;
        boolean contains = true;
        if (example.getProjectId() == null) {
            contains = contains(example.getName(), example.getUserId());
        } else {
            contains = contains(example.getProjectId(), example.getName(), example.getUserId());
        }
        if (contains) return null;
        entityMap.put(example.getId(), example);
        return example;
    }

    @Nullable
    @Override
    public Task merge(@NotNull final Task example) {
        if (example.getUserId() == null) return null;
        if (example.getName() == null) return null;
        if (example.getProjectId() == null) return null;
        final @Nullable Task oldTask = findOne(example.getName(), example.getProjectId(), example.getUserId());
        if (oldTask == null) return null;
        oldTask.setName(example.getName());
        oldTask.setDescription(example.getDescription());
        oldTask.setDateStart(example.getDateStart());
        oldTask.setDateFinish(example.getDateFinish());
        oldTask.setProjectId(example.getProjectId());
        oldTask.setUserId(example.getUserId());
        persist(example);
        return oldTask;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String taskName) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String taskName,
                       @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String taskName,
                       @NotNull final String projectId,
                       @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(@NotNull final String userId) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(@NotNull final String userId,
                                               @NotNull final String projectId) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() == null ||
                    !(entry.getValue().getProjectId().equals(projectId))) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @Override
    public boolean contains(@NotNull final String entityName) {
        return false;
    }

    public boolean contains(@NotNull final String taskName,
                            @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

    public boolean contains(@NotNull final String projectId,
                            @NotNull final String taskName,
                            @NotNull final String userId) {
        for (Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null &&
                    !entry.getValue().getProjectId().equals(projectId)) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

}
